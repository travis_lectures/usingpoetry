#!/usr/bin/env python3

import datetime
import argparse

frog = r'''
                            ,-.
                        _,-' - `--._
                      ,'.:  __' _..-)
                    ,'     /,o)'  ,'
                   ;.    ,'`-' _,)
                 ,'   :.   _.-','
               ,' .  .    (   /
              ; .:'     .. `-/
            ,'       ;     ,'
         _,/ .   ,      .,' ,
       ,','     .  .  . .\,'..__
     ,','  .:.      ' ,\ `\)``
     `-\_..---``````-'-.`.:`._/
     ,'   '` .` ,`- -.  ) `--..`-..
     `-...__________..-'-.._  \
        ``--------..`-._ ```
                     ``    SSt
'''

def run(force=False):
    day_of_week = datetime.date.today().weekday()
    if day_of_week == 2 or force: # it is wednesday my dudes
        print("It is wednesday my dudes")
        print(frog)
    else:
        print("You're gonna have to wait till Wednesday")


if __name__ == '__main__':
    parser = argparse.ArgumentParser("WednesdayFrog")
    parser.add_argument('-f', '--force', action='store_true', help='Make it wednesday whenever you want')
    args = parser.parse_args()
    run(force=args.force)