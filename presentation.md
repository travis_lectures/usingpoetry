# Poetry for your Python projects

## Why?

You can use `venv` to create and manage environments.

You can use `pip` to install and manage dependencies

You can use `setuptools` (or `build`) to create and manage wheels (packages)

But these are separate tools, and although they're great tools for all sorts of projects and allow you to use as much 
of them as you want, they're still treated as separate entities when they very often need to work together.
Poetry provides one tool that can preform all these tasks and provide a (mostly) streamlined experience.

## The basics:

### Environments

Typically, you use a virtual environment for Python projects to isolate your development from a system's environment. 
This helps with both separating dependencies from different projects and allows using different versions of Python 
for development without messing up your system's install of Python.

### Dependencies

Unless you're building everything from scratch, you typically use 3rd party libraries in development projects. 

Web development?
- Django
- Flask
- FastAPI

Data analytics?
- Pandas
- SciPy

Working with AWS?
- Boto3

The list goes on. To add to it, your dependencies have their own dependencies, which have to be installed along with 
whatever you're using. Most of these 3rd party tools are just a `pip install` away, but woe to the developer that has 
a version conflict from any of these dependencies (foreshadowing).

### Distributions

Distributing your code to where it needs to be is typically the most bespoke operation out of the three.
Library writers want others to use their packages, app developers need their applications to run, 
and data scientist just need the results of their work saved so they can repeat any analysis. 
Even within these different use cases the needs of the developer may be different: 
this is especially true for the app dev. Python has its expected patterns for much of how this works with packages, 
and with little effort you can get this to work for whatever your use case is.

### How these 3 things tie together

Your environment, dependencies, and distributions are linked together, affecting each other in ways that make it hard 
to decouple. With environments and dependencies, the links are pretty easy to see: you install dependencies into your 
environment (whether it's virtual or not). Distributions tie into these in different ways: 
certainly your project dependencies need to be accounted for when distributing your code. 
Environments can also have an effect on distributions: if you alter PYTHONPATH for your environment, 
your distribution is probably going to have issues if installed in another environment and that change wasn't taken 
into consideration.

### How Poetry helps manage these 3 things

Poetry provides tooling to help with all of these considerations for your project. 
Upon creating a project with Poetry, your project will

1) have a virtual environment made for it
2) provide an easy way to install and manage dependencies for your project
3) provide a simple-to-use build tool that creates distributable packages of your project

However, Poetry isn't perfect. Like much of the tooling for Python, it's very opinionated. 
However, for some of the small stuff there are workarounds. 

## Basic setup of a new project with Poetry

### First things first: get Poetry

https://python-poetry.org/docs/#installation

There's several different ways to get Poetry on your system. My suggestion: either use the `pipx` install 
or official install. 

### Adding Poetry to a project

New from scratch project?

`poetry new`

Already have some code and you want to introduce poetry afterward?

`poetry init`

Both of these commands, after answering some questions, will produce a `pyproject.toml` file. 
For a reference on what this file does (it's not specific to Poetry), please see 
https://packaging.python.org/en/latest/guides/writing-pyproject-toml/.

## Using Poetry to help manage your Python project

### Managing your project's environment

When Poetry is first initialized for your project, a virtual environment will be created for your project. 
To use this environment locally, `poetry shell` is a handy way to do so 
(we'll cover how `source .venv/bin/activate` can do the same thing in a bit)

To see information on your current environment being used, `poetry env info` can be a useful tool.

### Adding dependencies

To add a dependency to your project, use `poetry add`. This will add the dependency to `pyproject.toml` and 
install the dependency into your virtual environment. You may have noticed that this also created a `poetry.lock` file.

### What the heck is that?

Managing dependencies across different environments can be a troublesome prospect. Making sure that your dependencies 
work well together and don't have any conflicts is troublesome enough on its own, as `pip` didn't have a good 
dependency resolver on its own (this is a different case now, but I didn't know that until preparing this presentation).
And once you have dependencies that work well together, getting consistency with these dependencies was a nightmare
(sort of. Mostly because developers weren't diligent enough. But that's a different story 😉). 
Poetry's solution to these problems is using a lock file. By adding a dependency to a project with `poetry add`, 
Poetry verifies that the listed dependency (at the listed version, which is the newest unless specified) resolves 
any dependency conflicts and records the exact version used into the `poetry.lock` file. 
When this project is put into source control and pulled by another developer, the lock file provides a consistent 
set of dependencies any other dev's development environment.

### Installing dependencies

So you cloned a repo with a `poetry.lock` file in it already, and you want to get your environment setup? 
Use `poetry install`, and it'll create an environment if one doesn't exist and install the dependencies as well 
as the current project into it. Don't want to install your project into the environment? 
Use `poetry install --no-root`. 

### Specifying and Updating dependencies

When you use `poetry add` to add a dependency to your project, you may have noticed it shows up in your `pyproject.toml` 
file with a version number and a caret character. Poetry allows a form of constraint versioning to let a developer 
control what versions are allowed within the lock file. For an overview of what the constraints are, please see 
https://python-poetry.org/docs/dependency-specification/

Poetry works with the constraints provided by the developer to resolve dependencies when creating the lock file. 
To use updated versions of dependencies that still work together while also adhering to the version constraints, 
use the `poetry update` command.

### Building your project

Poetry has a pretty strong opinion on what you should be doing when using it to create a distributable build: 
you should be building packages. Poetry provides simple build tooling to achieve this: create a package by using 
`poetry build`. 

If you have a previous experience with `setup.py` or other build tools, you can change that in the `pyproject.toml` file
under the `[build-system]` section.

This may not be what you're looking for (I just want my code in this directory!) There's a broader discussion for a 
different time, but I'll leave you with my thoughts on it: If it's a python package, it can be pretty much put anywhere. 

### Providing entrypoints for your project

Want to run your project with one simple call to a script, but not sure how to bundle it properly with packages?
Within the `pyproject.toml` file, there's a section you can add that will add console scripts for your application: 
`[tool.poetry.scripts]`.


## What can't it do? (or what is it not super awesome at)

So the biggest problem that many people will face trying to pick up Poetry is the fact that it's VERY opinionated on 
how stuff should be done. In an attempt to provide structure upon which standards can be set up, 
it interferes with one of the most powerful aspects of Python: its flexibility. However, knowing is have the battle, 
so I've outlined some of the specific problems I've seen in dealing with Poetry, and later provide some fixes.

### Project setup

Poetry has an expectation for how a project should be set up:
<pre>
pythonProject/
├── pyproject.toml
├── app.py
└── pythonProject
    ├── __init__.py
    └── module_a.py
</pre>

Notice that pythonProject is a package which sits at the root of the project and has the same name as the 
project directory. These 2 conditions are expected out of a default Poetry project, which I find a bit too constrictive.
Solution: use the `[packages]` section of your `pyproject.toml` file to tell poetry (and anything else that reads 
`pyproject.toml`) what the actual package should include. The following stub will support the following structure:
```toml
[tool.poetry]
# ...
packages = [
    { include = "src/**" },
]
```
<pre>
pythonProject/
├── pyproject.toml
└── src
    ├── app.py
    └── util
        ├── __init__.py
        └── module_a.py
</pre>

### Adding scripts can be tricky

While adding an entrypoint using the `[scripts]` section looks very similar to using the entrypoint in setuptools, 
it acts more like the old style scripts tag in setup. This can cause some odd behavior with specifying what you to 
actually run. My suggestion? I do this by hand, but either install your package into your dev environment and see if you
trigger the script by its name there, or install it with `pipx` and see if you get the same outcome. 
I've not found what fundamentally goes wrong here, so unfortunately my best advice is "play around until it works"

### Working with containers

The intuitive way of working with containers doesn't work very well with Poetry projects. Simply loading the entire 
project into a container isn't that hard, but then everytime you change code and reload it the container invalidates 
the cache for the whole container, including Poetry itself. (I don't know of a container right now set up to be 
used with Poetry development). There's a couple of ways around this limitation:
- Install dependencies first, then copy code
  - see https://python-poetry.org/docs/faq/#poetry-busts-my-docker-cache-because-it-requires-me-to-copy-my-source-files-in-before-installing-3rd-party-dependencies
- Multistage build
  - see https://stackoverflow.com/a/57886655 (There is other good information in this SO)

## Tips I learnt to make things easier

- You may have noticed that I said a virtual environment is created for you whenever you make a new project using 
Poetry, but you might be wondering where it went. By default, it moves it to some directory set up to be a cache for
Poetry. However, I don't like this: I want my virtual environments where I can see them (and poke and prod them
when they give me trouble). To configure Poetry to do this, change the `virtualenvs.in-project` config option with
`poetry config virtualenvs.in-project true`. This defaults to the global configuration, so all projects afterward 
will have their virtual environments created at the project root instead of the cache directory.
  - Except for the first one you made 😆. You'll want to remove the old one with `poetry env remove {env}`. To get that
  {env} arg, use `poetry env list` to get a list of them. You can then create with a simple `poetry install`.
- Learn to use the dependency groups early, as this is how you can add development tools to the project without 
shipping them as part of your code. See https://python-poetry.org/docs/managing-dependencies/#optional-groups for a 
rundown on how this works. Unlike pipenv, you can have any number of dependency groups, with any name (dev included)
- Multi project repos: There's a couple of different ways to handle this. ATM I'm using a Poetry project with a 
separate `pyproject.toml` for each subproject, and changing my IDE environment for each one. I'll have to play 
around with using dependency groups and multiple virtual environments for multiple project repos in the future, 
but I don't have any info on that for now.
